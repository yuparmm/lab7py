import datetime

class Person:
    def __init__(self, surname, first_name, birth_date, nickname=None):
        self.surname = surname
        self.first_name = first_name
        self.birth_date = datetime.datetime.strptime(birth_date, "%Y-%m-%d").date()
        self.nickname = nickname

    def age(self):
        today = datetime.date.today()
        age = today.year - self.birth_date.year - ((today.month, today.day) < (self.birth_date.month, self.birth_date.day))
        return age

    def fullname(self):
        if self.nickname:
            return f"{self.surname} {self.first_name} ({self.nickname})"
        else:
            return f"{self.surname} {self.first_name}"

def modifier(input_filename, output_filename):
    modified_lines = []
    with open(input_filename, 'r') as file:
        lines = file.readlines()

    for line in lines:
        if line.strip():
            parts = line.strip().split()
            surname = parts[0]
            first_name = parts[1]
            birth_date = parts[2]
            nickname = parts[3] if len(parts) > 3 else None
            person = Person(surname, first_name, birth_date, nickname)
            full_name = person.fullname()
            age = person.age()
            modified_lines.append(f"{line.strip()}\n")
            modified_lines.append(f"Повне ім'я: {full_name}\n")
            modified_lines.append(f"Вік: {age} років\n\n")

    with open(output_filename, 'w') as file:
        file.writelines(modified_lines)

modifier("zmist.txt", "zmist2.txt")


